function sendAjaxRequest()
{

	var personId = personSelected();

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("content").innerHTML = this.responseText;
		}
	}

	request.open('GET', 'ajax.php?id='+personId, true);

	request.send();
}

function personSelected()
{
	var option = document.getElementById("person");
	var val = option.options[option.selectedIndex].value;
	return val;	
}

function personData()
{
	var selectedOption = document.getElementById("person-data");
	var value = selectedOption.options[selectedOption.selectedIndex].value;

	var req = new XMLHttpRequest();

	req.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("person-content-data").innerHTML = this.responseText;
		}
	}

	req.open('GET', 'ajax_example.php?id='+value, true);

	req.send();
}

function showUserData()
{
	var requestObject = document.getElementById("selected-user");
	var userId = requestObject.options[requestObject.selectedIndex].value;

	var data = new XMLHttpRequest();

	data.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("container").innerHTML = this.responseText;
		}
	}

	data.open('GET', 'include/ajax_data.php?id='+userId, true);

	data.send();
}

function getUserContentData()
{
	var foo = document.getElementById("user-content");
	var userIdData = foo.options[foo.selectedIndex].value;

	var reqObject = new XMLHttpRequest();

	reqObject.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("user-data-container").innerHTML = this.responseText;
		}
	}

	reqObject.open('GET', 'boot/app/ajax/ajax_example_test.php?id='+userIdData, true);

	reqObject.send();
}

function callAjax()
{
	var x = new XMLHttpRequest();

	x.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("ajax-content").innerHTML = this.responseText;
		}
	}

	x.open('POST', 'ajax_content.php', true);

	x.send();
}

/* Ajax - Sort examples */

function getAllUsers()
{
	var hidden = "all";

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("users-container").innerHTML = this.responseText;
		}
	}

	request.open("GET", "request.php?sort="+hidden, true);

	request.send();
}

function sortUserData()
{
	var hidden = "id";

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("users-container").innerHTML = this.responseText;
		}
	}

	request.open("GET", "request.php?sort="+hidden, true);

	request.send();
}

function sortUserDataByIdAsc()
{
	var hidden = "asc";

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("users-container").innerHTML = this.responseText;
		}
	}

	request.open("GET", "request.php?sort="+hidden, true);

	request.send();
}

function sortUserDataByNameAsc()
{
	var hidden = "nasc";

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("users-container").innerHTML = this.responseText;
		}
	}

	request.open("GET", "request.php?sort="+hidden, true);

	request.send();
}

function sortUserDataByNameDesc()
{
	var hidden = "ndesc";

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("users-container").innerHTML = this.responseText;
		}
	}

	request.open("GET", "request.php?sort="+hidden, true);

	request.send();
}

function sortUserDataByDateDesc()
{
	var hidden = "ddesc";

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("users-container").innerHTML = this.responseText;
		}
	}

	request.open("GET", "request.php?sort="+hidden, true);

	request.send();
}

function sortUserDataByDateAsc()
{
	var hidden = "dasc";

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("users-container").innerHTML = this.responseText;
		}
	}

	request.open("GET", "request.php?sort="+hidden, true);

	request.send();
}

function sortInfo(property)
{
	var hidden = property;

	var request = new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("users-container").innerHTML = this.responseText;
		}
	}

	request.open("GET", "request.php?sort="+hidden, true);

	request.send();
}

/* ONKEYUP EXAMPLE */

function onChangeData()
{
	var inputData = document.getElementById("key-value").value;

	var req = new XMLHttpRequest();

	req.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			document.getElementById("user-data-element").innerHTML = this.responseText;
		}
	}

	req.open("GET", "key.php?el="+inputData, true);

	req.send();
}