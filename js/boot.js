$(document).ready(function(){

	/* Ajax php sort examples */

	function showUserInfoData(info)
	{
		alert(info);
	}

	$("button#test").click(function(){
		showUserInfoData('afqwfqwfqwrqwrq');
	});

	function ajaxRequestInfo(x)
	{
		var information = x;

		$.ajax({
			url: "user.php",
			method: "GET",
			data: {sort: information},
			success: function(){
				console.log('Success');
			},
			error: function(){
				console.log('Error');
			},
			complete: function(){
				console.log('Complete');
			}
		}).done(function(response){
			$("div#user-data-content").append(response);
		});
	}

	$("button#all-info").click(function(){
		ajaxRequestInfo("all");
	});

	$("button#id-user-info").click(function(){
		ajaxRequestInfo("id");
	});

	$("button#id-user-info-asc").click(function(){
		ajaxRequestInfo("asc");
	});

	$("button#name-info-desc").click(function(){
		ajaxRequestInfo("ndesc");
	});

	$("button#name-info-asc").click(function(){
		ajaxRequestInfo("nasc");
	});

	$("button#date-info-desc").click(function(){
		ajaxRequestInfo("ddesc");
	});

	$("button#date-info-asc").click(function(){
		ajaxRequestInfo("dasc");
	});

	$("button#show-all-users").click(function(){
		$.ajax({
			url: "user.php",
			method: "GET",
			data: {sort: "all"},
			success: function(){
				console.log('Success');
			},
			error: function(){
				console.log('Error');
			},
			complete: function(){
				console.log('Complete');
			}
		}).done(function(response){
			$("div#user-data-content").append(response);
		});
	});

	$("button#id-desc-sort").click(function(){
		$.ajax({
			url: "user.php",
			method: "GET",
			data: {sort: "id"},
			success: function(){
				console.log('Success');
			},
			error: function(){
				console.log('Error');
			},
			complete: function(){
				console.log('Complete');
			}
		}).done(function(response){
			$("div#user-data-content").append(response);
		});
	});

	$("button#id-asc-sort").click(function(){
		$.ajax({
			url: "user.php",
			method: "GET",
			data: {sort: "asc"},
			success: function(){
				console.log('Success');
			},
			error: function(){
				console.log('Error');
			},
			complete: function(){
				console.log('Complete');
			}
		}).done(function(response){
			$("div#user-data-content").append(response);
		});
	});

	$("button#name-desc-sort").click(function(){
		$.ajax({
			url: "user.php",
			method: "GET",
			data: {sort: "ndesc"},
			success: function(){
				console.log('Success');
			},
			error: function(){
				console.log('Error');
			},
			complete: function(){
				console.log('Complete');
			}
		}).done(function(response){
			$("div#user-data-content").append(response);
		});
	});

	$("button#name-asc-sort").click(function(){
		$.ajax({
			url: "user.php",
			method: "GET",
			data: {sort: "nasc"},
			success: function(){
				console.log('Success');
			},
			error: function(){
				console.log('Error');
			},
			complete: function(){
				console.log('Complete');
			}
		}).done(function(response){
			$("div#user-data-content").append(response);
		});
	});

	$("button#date-desc-sort").click(function(){
		$.ajax({
			url: "user.php",
			method: "GET",
			data: {sort: "ddesc"},
			success: function(){
				console.log('Success');
			},
			error: function(){
				console.log('Error');
			},
			complete: function(){
				console.log('Complete');
			}
		}).done(function(response){
			$("div#user-data-content").append(response);
		});
	});

	$("button#date-asc-sort").click(function(){
		$.ajax({
			url: "user.php",
			method: "GET",
			data: {sort: "dasc"},
			success: function(){
				console.log('Success');
			},
			error: function(){
				console.log('Error');
			},
			complete: function(){
				console.log('Complete');
			}
		}).done(function(response){
			$("div#user-data-content").append(response);
		});
	});

	/* examples end */

	/* Ajax example */

	$("button#btn-jquery-ajax-test").click(function(){
		$.ajax({
			method: "GET",
			url: "foo.php",
			data: {id: 1, name: "John"},
			complete: function(data){
				console.log('Completed');
				console.log(data);
			},
			error: function(){
				console.log('Error happened');
			},
			success: function(){
				console.log('Success on ajax test');
			}
		}).done(function(data){ // data is foo.php content that has been printed with echo function
			console.log('So far so good.');
			/* $("div#jquery-ajax-data-container").append("<h1>Patrick Bateman</h1>"); */
			$("div#jquery-ajax-data-container").append(data);
		}).fail(function(){
			console.log('Fail while sending request');
		});
	});

	/* example end */

	/* Ajax test */

	$("select#person-info").change(function(){

		var userId = $(this).val();

		$.ajax({
			method: "GET",
			url: "bar.php",
			data: {id: userId},
			success: function(){
				console.log('ALL GOOD');
			},
			error: function(){
				console.log('ALL BAD');
			}
		}).done(function(data){
			$("div#person-info-container").append(data);
		});

	});

	/* test end */

	$("p").click(function(){
		$(this).hide();
	});

	$("button#ajax-jquery-btn").click(function(){
		$("div#jquery-data-container").load("jquery_foo.php");
	});

	$("button#ajax-jquery-btn").click(function(){

		$("div#jquery-data-container-two").load("include/jquery_example.php #foo", function(responseTxt, statusTxt, xhr){
			
			if(statusTxt == "success"){
				console.log(responseTxt);
				console.log(statusTxt);
				console.log(xhr.status);
			}

			if(statusTxt == "error"){
				console.log('All bad');
			}

		});

	});

	$("button#ajax-jquery-btn").click(function(){
		$("div#jquery-data-container-three").load("boot/app/ajax/jquery_bar.php");
	});

	$("button#ajax-jquery-btn").click(function(){
		$.get("jquery_foo.php", function(data, status){
			console.log(data);
			console.log(status);
		});
	});

	$("button#ajax-jquery-btn").click(function(){
		$.post("jquery_foo.php", function(data, status){
			console.log(data);
			console.log(status);
		});
	});

	/* ONKEYUP EXAMPLES */

	function onChangeShowData()
	{
		var rez = $("input#jq-key-value").val();
		
		$.ajax({
			url: "include/jq_key.php",
			method: "GET",
			data: { el: rez },
			success: function(){
				console.log('Success while sending data');
			},
			error: function(){
				console.log('Error while sending data');
			}	
		}).done(function(responseData){
			$("div#jq-user-data-element").append(responseData);
		});

	}

	$("input#jq-key-value").keyup(function(){
		onChangeShowData();
	});

	/* EXAMPLES END */

});