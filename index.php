<!DOCTYPE html>
<html>
	<head>
		<title>Ajax</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>

		<hr />

		<h4>AJAX JQUERY ONKEYUP EXAMPLE</h4>

		<form>
			<input type="text" name="jq-user-data" id="jq-key-value" />
		</form>

		<div id="jq-user-data-element">
			
		</div>

		<hr />

		<h4>ONKEYUP EXAMPLE</h4>

		<form>
			<input id="key-value" type="text" name="user-data" onkeyup="onChangeData()" />
		</form>

		<div id="user-data-element">
			
		</div>

		<hr />

		<button id="show-all-users" style="padding: 5px;">ALL USERS</button>

		<button id="id-desc-sort" style="padding: 5px;">SORT BY ID DESC</button>

		<button id="id-asc-sort" style="padding: 5px;">SORT BY ID ASC</button>

		<button id="name-desc-sort" style="padding: 5px;">SORT BY NAME DESC</button>

		<button id="name-asc-sort" style="padding: 5px;">SORT BY NAME ASC</button>

		<button id="date-desc-sort" style="padding: 5px;">SORT BY DATE DESC</button>

		<button id="date-asc-sort" style="padding: 5px;">SORT BY DATE ASC</button>

		<button id="test" style="padding: 5px;">TEST</button>

		<hr />

		<button id="all-info" style="padding: 5px;">ALL USERS</button>

		<button id="id-user-info" style="padding: 5px;">SORT BY ID DESC</button>

		<button id="id-user-info-asc" style="padding: 5px;">SORT BY ID ASC</button>

		<button id="name-info-desc" style="padding: 5px;">SORT BY NAME DESC</button>

		<button id="name-info-asc" style="padding: 5px;">SORT BY NAME ASC</button>

		<button id="date-info-desc" style="padding: 5px;">SORT BY DATE DESC</button>

		<button id="date-info-asc" style="padding: 5px;">SORT BY DATE ASC</button>

		<div id="user-data-content">
			
		</div>

		<hr />

		<?php

			$host = 'localhost';
			$username = 'root';
			$password = 'aurora';
			$db = 'foo_bar';

			$conn = mysqli_connect($host, $username, $password, $db);

			$sql = "SELECT * FROM users";

			$records = mysqli_query($conn, $sql);

			echo "<table border='1'>
				      <tr class='table-head'>
					      <td>ID</td>
					      <td>NAME</td>
					      <td>EMAIL</td>
					      <td>PASSWORD</td>
					      <td>CREATED AT</td>
					  </tr>";

			while($user = mysqli_fetch_object($records)){
				echo "<tr>
					  <td>$user->userID</td>
					  <td>$user->name</td>
					  <td>$user->email</td>
					  <td>$user->password</td>
					  <td>$user->created_at</td>
					  </tr>";
			}

			echo "</table>";
			
		?>

		<button id="btn-sort-id">SORT BY ID</button> 
		<button id="btn-sort-name">SORT BY NAME</button> 
		<button id="btn-sort-date">SORT BY DATE</button>

		<hr />

		<button style="padding: 15px;" onclick="getAllUsers()">ALL USERS</button>

		<button style="padding: 15px;" onclick="sortUserData()">SORT BY ID DESC</button>

		<button style="padding: 15px;" onclick="sortUserDataByIdAsc()">SORT BY ID ASC</button>

		<button style="padding: 15px;" onclick="sortUserDataByNameAsc()">SORT BY NAME ASC</button>

		<button style="padding: 15px;" onclick="sortUserDataByNameDesc()">SORT BY NAME DESC</button>

		<button style="padding: 15px;" onclick="sortUserDataByDateDesc()">SORT BY DATE DESC</button>

		<button style="padding: 15px;" onclick="sortUserDataByDateAsc()">SORT BY DATE ASC</button>

		<hr />

		<button style="padding: 15px;" onclick="sortInfo('all')">ALL USERS</button>

		<button style="padding: 15px;" onclick="sortInfo('id')">SORT BY ID DESC</button>

		<button style="padding: 15px;" onclick="sortInfo('asc')">SORT BY ID ASC</button>

		<button style="padding: 15px;" onclick="sortInfo('nasc')">SORT BY NAME ASC</button>

		<button style="padding: 15px;" onclick="sortInfo('ndesc')">SORT BY NAME DESC</button>

		<button style="padding: 15px;" onclick="sortInfo('ddesc')">SORT BY DATE DESC</button>

		<button style="padding: 15px;" onclick="sortInfo('dasc')">SORT BY DATE ASC</button>

		<div id="users-container">
			
		</div>

		<hr /> 

		<form>
			<select name="jq-person" id="person-info">
				<option value="">Select a person:</option>
				<option value="1">Jovan</option>
				<option value="2">Damjan</option>
				<option value="3">Jelena</option>
				<option value="4">Stefan</option>
			</select>
		</form>

		<div id="person-info-container"></div>

		<hr />

		<button id="btn-jquery-ajax-test">GO</button>

		<div id="jquery-ajax-data-container"></div>

		<hr />

		<form>
			<select name="user" id="person" onchange="personSelected()">
				<option value="">Select person:</option>
				<option value="1">Jovan</option>
				<option value="2">Damjan</option>
				<option value="3">Jelena</option>
				<option value="4">Stefan</option>
			</select>
		</form>

		<button onclick="sendAjaxRequest()" id="btn">GO</button>

		<div id="content">
			
		</div>

		<hr />

		<form>
			<select name="user-name" id="person-data" onchange="personData()">
				<option value="">Select a person:</option>
				<option value="1">Jovan</option>
				<option value="2">Damjan</option>
				<option value="3">Jelena</option>
				<option value="4">Stefan</option>
			</select>
		</form>

		<div id="person-content-data">
			
		</div>

		<hr />

		<form>
			<select name="user-option" id="selected-user" onchange="showUserData()">
				<option value="">Select a person:</option>
				<option value="1">Jovan</option>
				<option value="2">Damjan</option>
				<option value="3">Jelena</option>
				<option value="4">Stefan</option>
			</select>
		</form>

		<div id="container">
			
		</div>

		<hr />

		<form>
			<select name="select-data" id="user-content" onchange="getUserContentData()">
				<option value="">Select a person:</option>
				<option value="1">Jovan</option>
				<option value="2">Damjan</option>
				<option value="3">Jelena</option>
				<option value="4">Stefan</option>
			</select>
		</form>

		<div id="user-data-container">
			
		</div>

		<hr />

		<button onclick="callAjax()">DATA</button>

		<div id="ajax-content">
			
		</div>

		<hr />

		<p>Test</p>

		<p>Test</p>

		<p>Test</p>

		<hr />

		<button id="ajax-jquery-btn">RUN</button>

		<div id="jquery-data-container">
			
		</div>

		<div id="jquery-data-container-two">
			
		</div>

		<div id="jquery-data-container-three">
			
		</div>

		<div id="jquery-data-container-four">
			
		</div>

		<script src="js/app.js"></script>
		<script src="js/jquery.js"></script>
		<script src="js/boot.js"></script>
	</body>
</html>