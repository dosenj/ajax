<?php

function getSortValue()
{
	return $_GET['sort'];
}

function show_users()
{
	$sort_value = getSortValue(); 

	$host = 'localhost';
	$username = 'root';
	$password = 'aurora';
	$db = 'foo_bar';

	$conn = mysqli_connect($host, $username, $password, $db);

	if($sort_value == 'all'){
		$sql = "SELECT * FROM users";
	}

	if($sort_value == 'id'){
		$sql = "SELECT * FROM users ORDER BY userID DESC";
	}

	if($sort_value == 'asc'){
		$sql = "SELECT * FROM users ORDER BY userID";
	}

	if($sort_value == 'nasc'){
		$sql = "SELECT * FROM users ORDER BY name";
	}

	if($sort_value == 'ndesc'){
		$sql = "SELECT * FROM users ORDER BY name DESC";
	}

	if($sort_value == 'ddesc'){
		$sql = "SELECT * FROM users ORDER BY created_at DESC";
	}

	if($sort_value == 'dasc'){
		$sql = "SELECT * FROM users ORDER BY created_at";
	}

	$records = mysqli_query($conn, $sql);

	echo "<table border='1'>
		      <tr class='table-head'>
			      <td>ID</td>
			      <td>NAME</td>
			      <td>EMAIL</td>
			      <td>PASSWORD</td>
			      <td>CREATED AT</td>
			  </tr>";

	while($user = mysqli_fetch_object($records)){
		echo "<tr>
			  <td>$user->userID</td>
			  <td>$user->name</td>
			  <td>$user->email</td>
			  <td>$user->password</td>
			  <td>$user->created_at</td>
			  </tr>";
	}

	echo "</table>";
}

show_users();